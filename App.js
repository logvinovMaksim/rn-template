
// outsource dependencies
import React from 'react';
import { Provider } from 'react-redux';

// local dependencies
import navigationService from './src/services/navigation';
import store from './src/store';
import AppScreens from './src/navigation/Routes';

export default function App () {
    return (
        <Provider store={store}>
            <AppScreens
                ref={navigatorRef => {
                    navigationService.setTopLevelNavigator(navigatorRef);
                }}
            />
        </Provider>
    );
}
