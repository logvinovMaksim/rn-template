
// outsource dependencies
import { NavigationActions } from 'react-navigation';

let navigator;

// Use this only with the top-level (root) navigator of your app
// https://reactnavigation.org/docs/en/navigating-without-navigation-prop.html
function setTopLevelNavigator (navigatorRef) {
    navigator = navigatorRef;
}

function navigate (routeName, params) {
    navigator.dispatch(
        NavigationActions.navigate({
            routeName,
            params,
        })
    );
}

// add other navigation functions that you need and export them
export default {
    navigate,
    setTopLevelNavigator,
};
