
// local dependencies
import { PUBLIC } from '../../actions/types';

const initial = {
    expectAnswer: false,
    errorMessage: null,
};

export default function (state = initial, action) {
    const { type } = action;
    switch (type) {
        case PUBLIC.START_SCREEN.START:
            state = { expectAnswer: true };
            break;
        case PUBLIC.START_SCREEN.SUCCESS:
            state = { expectAnswer: false };
            break;
        default:
            break;
    }

    return state;
}
