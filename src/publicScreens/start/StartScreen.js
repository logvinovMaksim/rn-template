
// outsource dependencies
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Field, reduxForm } from 'redux-form';
import PropTypes from 'prop-types';
import { StyleSheet, View, Button, TextInput } from 'react-native';
import Toast from 'react-native-root-toast';

// local dependencies
import Screen from '../../components/common/Screen';
import Text from '../../components/common/Text';
import { PUBLIC } from '../../actions/types';
import { APP_COLORS, APP_OFFSETS } from '../../styles/appThemeConstants';

const styles = StyleSheet.create({
    content: {
        flex: 1,
        justifyContent: 'center',
    },
    btnContainer: {
        display: 'flex',
    },
    toCameraBtn: {
        marginTop: APP_OFFSETS.TOP,
    },
    startRequestBtn: {
        marginTop: APP_OFFSETS.TOP,
    },
    userInput: {
        backgroundColor: APP_COLORS.POSITIVE_GREEN
    },
    submitBtn: {
        marginTop: APP_OFFSETS.TOP
    },
    toastBtn: {
        marginTop: APP_OFFSETS.TOP
    },
});

const renderInput = ({
    input: { onBlur, onChange, onFocus, value },
    meta: { error, touched, valid }
}) => {
    return [
        (<TextInput key="field" style={styles.userInput} onChangeText={onChange} />),
        (!valid && touched && <Text key="error-message">{error}</Text>)
    ];
};

renderInput.propTypes = {
    input: PropTypes.object.isRequired,
    meta: PropTypes.object.isRequired,

};

class StartScreen extends Component {
    constructor () {
        super();
        this.state = {
            formVisible: true,
        };
    }

    submitForm = (values) => {
        console.info('submitting form', values);
        this.setState({ formVisible: false });
    };

    goToSupportScreen = ({ userStatus = 'Still unknown' }) => this.props.navigation.navigate('SupportScreen', { userStatus });

    goToCameraScreen = ({ userStatus = 'Still unknown' }) => this.props.navigation.navigate('CameraScreen', { userStatus });

    startRequest = () => this.props.startRequest();

    toastExample = () => {
        Toast.show(('Welcome to react native app'), {
            duration: Toast.durations.LONG,
            position: Toast.positions.BOTTOM,
        });
    };

    render () {
        const { handleSubmit } = this.props;
        const { formVisible } = this.state;
        return (
            <Screen>
                <View style={styles.content}>
                    <Text center variant="boldTitle">Welcome to React Native advanced template!</Text>
                    <Text center variant="subtitle">You currently at Start Screen</Text>
                    {
                        formVisible ? (
                            <View style={styles.container}>
                                <Text>Enter - Bob</Text>
                                <Field name="user" component={renderInput} />
                                <View style={styles.submitBtn}>
                                    <Button title="Submit" onPress={handleSubmit(this.submitForm)} />
                                </View>
                                <View style={styles.toastBtn}>
                                    <Button title="Toast" onPress={this.toastExample} />
                                </View>
                            </View>
                        ) : (
                            <View style={styles.btnContainer}>
                                <Button title="Support Screen" onPress={this.goToSupportScreen} />
                                <View style={styles.toCameraBtn}>
                                    <Button title="Camera Screen" onPress={this.goToCameraScreen} />
                                </View>
                                <View style={styles.startRequestBtn}>
                                    <Button title="Start request" onPress={this.startRequest} />
                                </View>
                            </View>
                        )
                    }
                </View>
            </Screen>
        );
    }
}

StartScreen.displayName = 'StartScreen';

StartScreen.propTypes = {
    navigation: PropTypes.object.isRequired,
    startRequest: PropTypes.func.isRequired,
    handleSubmit: PropTypes.func.isRequired,
};

export default reduxForm({
    form: 'auth',
    /**
     * @param { Object } values - named properties of input data
     * @param { Object } meta - information about form status
     * @returns { Object } - named errors
     * @function validate
     * @public
     */
    validate: (values, meta) => {
        const errors = {};
        // User
        if (!values.user) {
            errors.user = 'User name is required';
        } else if (values.user !== 'Bob') {
            errors.user = 'Invalid user data';
        }
        return errors;
    },
})(connect(
    state => ({ ...state.start }),
    dispatch => ({
        startRequest: () => dispatch({ type: PUBLIC.START_SCREEN.REQUEST }),
    })
)(StartScreen));
