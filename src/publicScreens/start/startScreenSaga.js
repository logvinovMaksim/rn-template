
// outsource dependencies
import { takeLatest, put } from 'redux-saga/effects';

//local dependencies
import { PUBLIC } from '../../actions/types';
import navigationService from '../../services/navigation';

function* startScreenFlow (action) {
    const { type, ...options } = action;
    try {
        yield put({ type: PUBLIC.START_SCREEN.SUCCESS });
    } catch (error) {
        yield put({ type: PUBLIC.START_SCREEN.ERROR, errorMessage: error.message });
    }
    yield put({ type: PUBLIC.START_SCREEN.FINISH });
    yield navigationService.navigate('SupportScreen', { userStatus: 'Authorized' });
}

/**
 * common request handler
 *
 * @public
 */
export default function* () {
    yield takeLatest(PUBLIC.START_SCREEN.REQUEST, startScreenFlow);
}
