
// outsource dependencies
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, View, Button } from 'react-native';
import { connect } from 'react-redux';

// local dependencies
import Screen from '../../components/common/Screen';
import Text from '../../components/common/Text';
import { PUBLIC } from '../../actions/types';
import { APP_OFFSETS } from '../../styles/appThemeConstants';

const styles = StyleSheet.create({
    content: {
        flex: 1,
        justifyContent: 'center',
    },
    supportRequestBtn: {
        marginTop: APP_OFFSETS.TOP,
    },
});

class SupportScreen extends PureComponent {
    goToStartScreen = ({ userStatus = 'Still unknown' }) => this.props.navigation.navigate('StartScreen', { userStatus });

    supportRequest = () => this.props.supportRequest();

    render () {
        return (
            <Screen>
                <View style={styles.content}>
                    <Text center variant="boldTitle">Welcome to React Native advanced template!</Text>
                    <Text center variant="subtitle">You currently at Support Screen</Text>
                    <Button title="Start Screen" onPress={this.goToStartScreen} />
                    <View style={styles.supportRequestBtn}>
                        <Button title="Start request" onPress={this.supportRequest} />
                    </View>
                </View>
            </Screen>
        );
    }
}

SupportScreen.displayName = 'SupportScreen';

SupportScreen.propTypes = {
    navigation: PropTypes.object.isRequired,
    supportRequest: PropTypes.func.isRequired,
};

export default connect(
    state => ({ ...state.support }),
    dispatch => ({
        supportRequest: () => dispatch({ type: PUBLIC.SUPPORT.REQUEST }),
    })
)(SupportScreen);
