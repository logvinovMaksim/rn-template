
// outsource dependencies
import { takeLatest, put } from 'redux-saga/effects';

//local dependencies
import { PUBLIC } from '../../actions/types';

function* supportFlow (action) {
    const { type, ...options } = action;
    try {
        yield put({ type: PUBLIC.SUPPORT.SUCCESS });
    } catch (error) {
        yield put({ type: PUBLIC.SUPPORT.ERROR, errorMessage: error.message });
    }
    yield put({ type: PUBLIC.SUPPORT.FINISH });
}

/**
 * common request handler
 *
 * @public
 */
export default function* () {
    yield takeLatest(PUBLIC.SUPPORT.REQUEST, supportFlow);
}
