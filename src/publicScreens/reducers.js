
// local dependencies
import camera from './camera/cameraScreenReducer';
import start from './start/startScreenReducer';
import support from './support/supportScreenReducer';

/**
 * connect all private reducers
 *
 * @public
 */

export default {
    camera,
    start,
    support,
};
