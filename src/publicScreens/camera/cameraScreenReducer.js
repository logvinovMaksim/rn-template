
// local dependencies
import { PUBLIC } from '../../actions/types';

const initial = {
    expectAnswer: false,
    errorMessage: null,
};

export default function (state = initial, action) {
    const { type } = action;
    switch (type) {
        case PUBLIC.CAMERA.START:
            state = { expectAnswer: true };
            break;
        case PUBLIC.CAMERA.SUCCESS:
            state = { expectAnswer: false };
            break;
        default:
            break;
    }

    return state;
}
