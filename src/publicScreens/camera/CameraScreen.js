
// outsource dependencies
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { RNCamera } from 'react-native-camera';
import { StyleSheet, Text, View, TouchableOpacity } from 'react-native';

// local dependencies
import { APP_COLORS } from '../../styles/appThemeConstants';
import { PUBLIC } from '../../actions/types';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: APP_COLORS.BLACK,
    },
    preview: {
        flex: 1,
        justifyContent: 'flex-end',
        alignItems: 'center',
    },
    capture: {
        flex: 0,
        backgroundColor: APP_COLORS.WHITE,
        borderRadius: 5,
        padding: 15,
        paddingHorizontal: 20,
        alignSelf: 'center',
        margin: 20,
    },
});

const cameraPermission = {
    title: 'Permission to use camera',
    message: 'We need your permission to use your camera',
    buttonPositive: 'Ok',
    buttonNegative: 'Cancel',
};

const audioPermission = {
    title: 'Permission to use audio recording',
    message: 'We need your permission to use your audio',
    buttonPositive: 'Ok',
    buttonNegative: 'Cancel',
};

class CameraScreen extends PureComponent {
    constructor () {
        super();
        this.state = {
            torchOn: false
        };
    }

    cameraRequest = () => this.props.cameraRequest();

    takePicture = async () => {
        if (this.camera) {
            const options = { quality: 0.5, base64: true };
            const data = await this.camera.takePictureAsync(options);
            console.info(data.uri);
        }
    };

    toggleTorch = () => {
        const { torchOn } = this.state;
        this.setState({ torchOn: !torchOn });
    };

    goBack = () => this.props.navigation.goBack();

    cameraRef = ref => this.camera = ref;

    render () {
        const { torchOn } = this.state;
        return (
            <View style={styles.container}>
                <RNCamera
                    ref={this.cameraRef}
                    style={styles.preview}
                    type={RNCamera.Constants.Type.back}
                    flashMode={torchOn ? RNCamera.Constants.FlashMode.torch : RNCamera.Constants.FlashMode.off}
                    androidCameraPermissionOptions={cameraPermission}
                    androidRecordAudioPermissionOptions={audioPermission}
                />
                <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
                    <TouchableOpacity onPress={this.takePicture} style={styles.capture}>
                        <Text> SNAP </Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={this.toggleTorch} style={styles.capture}>
                        <Text> Torch </Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={this.goBack} style={styles.capture}>
                        <Text> Exit </Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={this.cameraRequest} style={styles.capture}>
                        <Text> Camera flow </Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}

CameraScreen.propTypes = {
    navigation: PropTypes.object.isRequired,
    cameraRequest: PropTypes.func.isRequired,
};

CameraScreen.displayName = 'CameraScreen';

export default connect(
    state => ({ ...state.camera }),
    dispatch => ({
        cameraRequest: () => dispatch({ type: PUBLIC.CAMERA.REQUEST }),
    })
)(CameraScreen);
