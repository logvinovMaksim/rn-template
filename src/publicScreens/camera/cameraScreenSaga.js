
// outsource dependencies
import { takeLatest, put } from 'redux-saga/effects';

//local dependencies
import { PUBLIC } from '../../actions/types';

function* cameraFlow (action) {
    const { type, ...options } = action;
    try {
        yield put({ type: PUBLIC.CAMERA.SUCCESS });
    } catch (error) {
        yield put({ type: PUBLIC.CAMERA.ERROR, errorMessage: error.message });
    }
    yield put({ type: PUBLIC.CAMERA.FINISH });
}

/**
 * common request handler
 *
 * @public
 */
export default function* () {
    yield takeLatest(PUBLIC.CAMERA.REQUEST, cameraFlow);
}
