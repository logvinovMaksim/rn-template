
// outsource dependencies
import { fork } from 'redux-saga/effects';

// local dependencies
import camera from './camera/cameraScreenSaga';
import start from './start/startScreenSaga';
import support from './support/supportScreenSaga';

/**
 * connect all private sagas
 *
 * @public
 */
export default function* () {
    yield fork(camera);
    yield fork(start);
    yield fork(support);
}
