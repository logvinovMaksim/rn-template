
// outsource dependencies
import PropTypes from 'prop-types';
import React, { PureComponent } from 'react';
import _ from 'lodash';
import { withNavigationFocus, NavigationEvents } from 'react-navigation';
import { View, StyleSheet, BackHandler } from 'react-native';

// local dependencies
import { APP_COLORS, SCREEN_STYLES } from '../../styles/appThemeConstants';

const styles = StyleSheet.create({
    screenContainer: {
        flex: 1,
        display: 'flex',
        backgroundColor: APP_COLORS.THEME_COLOR_054,
    },
    screen: {
        ...SCREEN_STYLES.COMMON,
    },
    modalScreen: {
        ...SCREEN_STYLES.MODAL,
    },
});
// Avoid multiply screens back
const limitedGoBack = _.throttle(nav => nav.pop(), 200, { leading: true, trailing: false });

class Screen extends PureComponent {
    constructor () {
        super();
        this.state = {
            screenFocused: false,
        };
    }

    componentDidMount () {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
    }

    componentWillUnmount () {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);
    }

    handleScreenFocus = () => this.setState({ screenFocused: true });

    handleScreenUnfocus = () => this.setState({ screenFocused: false });

    handleBackPress = () => {
        const { navigation, onBackPress, allowDefaultBack, isFocused } = this.props;
        const { screenFocused } = this.state;
        if (!isFocused) { return; }
        if (typeof onBackPress === 'function') { onBackPress(); }
        if (allowDefaultBack && screenFocused) { limitedGoBack(navigation); }
    };

    render () {
        const { children, style, modal } = this.props;
        const screenStyles = modal ? styles.modalScreen : styles.screen;
        return (
            <View style={styles.screenContainer}>
                <View style={[screenStyles, style]}>
                    {children}
                </View>
                <NavigationEvents
                    onWillFocus={this.handleScreenFocus}
                    onDidFocus={this.handleScreenFocus}
                    onDidBlur={this.handleScreenUnfocus}
                />
            </View>
        );
    }
}

Screen.propTypes = {
    modal: PropTypes.bool,
    style: PropTypes.object,
    children: PropTypes.node,
    isFocused: PropTypes.bool,
    onBackPress: PropTypes.func,
    navigation: PropTypes.object,
    allowDefaultBack: PropTypes.bool,
};

Screen.defaultProps = {
    style: null,
    modal: false,
    children: null,
    navigation: null,
    isFocused: false,
    onBackPress: null,
    allowDefaultBack: false,
};

export default withNavigationFocus(Screen);
