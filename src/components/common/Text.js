
// outsource dependencies
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, Text as UIText } from 'react-native';

// local dependencies
import { APP_COLORS, DEFAULT_FONT_STYLES } from '../../styles/appThemeConstants';

export const textStyles = StyleSheet.create({
    title: {
        ...DEFAULT_FONT_STYLES,
        fontSize: 24,
    },
    boldTitle: {
        ...DEFAULT_FONT_STYLES,
        fontSize: 24,
        fontWeight: '500',
    },
    subtitle: {
        ...DEFAULT_FONT_STYLES,
        fontWeight: '500',
        fontSize: 14,
        letterSpacing: 0.75,
    },
    button: {
        ...DEFAULT_FONT_STYLES,
        fontWeight: '500',
        fontSize: 14,
        letterSpacing: 0.75,
    },
    h6: {
        ...DEFAULT_FONT_STYLES,
        fontSize: 20,
        fontWeight: '500',
        letterSpacing: 0.15,
    },
    h5: {
        ...DEFAULT_FONT_STYLES,
        fontSize: 24,
    },
    body: {
        ...DEFAULT_FONT_STYLES,
        fontSize: 16,
        letterSpacing: 0.44,
    },
    screenTitle: {
        ...DEFAULT_FONT_STYLES,
        fontWeight: '500',
        fontSize: 20,
        letterSpacing: 0,
    },
    caption: {
        ...DEFAULT_FONT_STYLES,
        fontSize: 12,
        letterSpacing: 0.44,
    },
    errorMessage: {
        ...DEFAULT_FONT_STYLES,
        fontSize: 10,
        letterSpacing: 0.44,
        color: APP_COLORS.NEGATIVE_RED,
    },
});

/**
 * Use for generating text style
 *
 * @param {object, array} style - possible specific text style.
 * @param {string} variant - set predefined text style.
 * @param {boolean} center - center text.
 * @param {string} color - set text color.
 * @returns {string} Generated text style.
 * @private
 */

const getTextStyle = (style, variant, center, color) => {
    const variantStyle = textStyles[variant] ? textStyles[variant] : 'body';
    const ts = center ? { textAlign: 'center' } : {};
    return StyleSheet.flatten([variantStyle, style, ts, color]);
};

class Text extends PureComponent {
    render () {
        const { children, style, variant, center, color } = this.props;
        const textStyle = getTextStyle(style, variant, center, color);
        return (
            <UIText style={textStyle}>
                {children}
            </UIText>
        );
    }
}

Text.propTypes = {
    children: PropTypes.node,
    variant: PropTypes.string,
    center: PropTypes.bool,
    color: PropTypes.string,
    style: PropTypes.oneOfType([
        PropTypes.object,
        PropTypes.array,
    ]),
};

Text.defaultProps = {
    children: null,
    variant: 'body',
    center: false,
    color: APP_COLORS.FONTS_COLOR,
    style: null,
};

export default Text;
