import color from 'color';

const THEME_LIGHT_COLOR = '#FAEEE4';
const THEME_COLOR = '#382A2C';
const WHITE = '#FFF';
const BLACK = '#000';
const THEME_DISABLED_COLOR = '#999999';
const POSITIVE_GREEN = '#32A03D';
const NEGATIVE_RED = '#FF5722';

/**
 * Use when you need color with opacity
 *
 * @param {string} colorValue - responsible for color.
 * @param {number} opacityValue - parameter affects opacity.
 * @returns {string} Generated color with opacity.
 * @private
 */
const createColorWithOpacity = (colorValue, opacityValue) => color(colorValue).alpha(opacityValue).string();

export const APP_COLORS = {
    THEME_LIGHT_COLOR,
    THEME_COLOR,
    WHITE,
    BLACK,
    THEME_DISABLED_COLOR,
    POSITIVE_GREEN,
    NEGATIVE_RED,
    THEME_COLOR_080: createColorWithOpacity(THEME_COLOR, 0.8),
    THEME_COLOR_054: createColorWithOpacity(THEME_COLOR, 0.54),
    WHITE_054: createColorWithOpacity(WHITE, 0.54),
    FONTS_COLOR: BLACK,
};

export const SCREEN_BORDER_RADIUS = 8;

export const COMMON_OFFSET = 8;
export const APP_OFFSETS = {
    LEFT: COMMON_OFFSET * 2,
    RIGHT: COMMON_OFFSET * 2,
    TOP: COMMON_OFFSET * 2,
    BOTTOM: COMMON_OFFSET * 2,
    HORIZONTAL: COMMON_OFFSET * 2,
    VERTICAL: COMMON_OFFSET * 2,
    DEFAULT: COMMON_OFFSET * 2,
};

const BASE_SCREEN_STYLE = {
    flex: 1,
    display: 'flex',
    padding: APP_OFFSETS.DEFAULT,
    backgroundColor: APP_COLORS.THEME_LIGHT_COLOR,
};

export const SCREEN_STYLES = {
    COMMON: {
        ...BASE_SCREEN_STYLE,
    },
    MODAL: {
        ...BASE_SCREEN_STYLE,
        borderTopLeftRadius: SCREEN_BORDER_RADIUS,
        borderTopRightRadius: SCREEN_BORDER_RADIUS,
    }
};

export const DEFAULT_FONT_STYLES = {
    color: APP_COLORS.FONTS_COLOR,
    fontStyle: 'normal',
    fontWeight: 'normal',
};

export const HEADER_STYLE = {
    BACKGROUND_COLOR: APP_COLORS.THEME_COLOR,
    TITLE_COLOR: APP_COLORS.WHITE_054,
};
