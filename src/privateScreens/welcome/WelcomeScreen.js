
// outsource dependencies
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, View, Button } from 'react-native';
import { connect } from 'react-redux';

// local dependencies
import { APP_OFFSETS } from '../../styles/appThemeConstants';
import { PRIVATE } from '../../actions/types';
import Screen from '../../components/common/Screen';
import Text from '../../components/common/Text';

const styles = StyleSheet.create({
    content: {
        flex: 1,
        justifyContent: 'center',
    },
    getInfoBtn: {
        marginTop: APP_OFFSETS.TOP,
    },
});

class WelcomeScreen extends PureComponent {
    welcomeRequest = () => this.props.welcomeRequest();

    render () {
        return (
            <Screen>
                <View style={styles.content}>
                    <Text center variant="boldTitle">Welcome to React Native advanced template!</Text>
                    <Text center variant="subtitle">You currently at Welcome Screen</Text>
                    <View style={styles.getInfoBtn}>
                        <Button title="Get Info" onPress={this.welcomeRequest} />
                    </View>
                </View>
            </Screen>
        );
    }
}

WelcomeScreen.displayName = 'WelcomeScreen';

WelcomeScreen.propTypes = {
    welcomeRequest: PropTypes.func.isRequired,
};

export default connect(
    state => ({ ...state.welcome }),
    dispatch => ({
        welcomeRequest: () => dispatch({ type: PRIVATE.WELCOME.REQUEST }),
    })
)(WelcomeScreen);
