
// outsource dependencies
import { takeLatest, put } from 'redux-saga/effects';

//local dependencies
import { PRIVATE } from '../../actions/types';

function* welcomeFlow (action) {
    const { type, ...options } = action;
    try {
        yield put({ type: PRIVATE.WELCOME.SUCCESS });
    } catch (error) {
        yield put({ type: PRIVATE.WELCOME.ERROR, errorMessage: error.message });
    }
    yield put({ type: PRIVATE.WELCOME.FINISH });
}

/**
 * common request handler
 *
 * @public
 */
export default function* () {
    yield takeLatest(PRIVATE.WELCOME.REQUEST, welcomeFlow);
}
