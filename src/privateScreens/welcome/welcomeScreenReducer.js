
// local dependencies
import { PRIVATE } from '../../actions/types';

const initial = {
    expectAnswer: false,
    errorMessage: null,
};

export default function (state = initial, action) {
    const { type } = action;
    switch (type) {
        case PRIVATE.WELCOME.START:
            state = { expectAnswer: true };
            break;
        case PRIVATE.WELCOME.SUCCESS:
            state = { expectAnswer: false };
            break;
        default:
            break;
    }

    return state;
}
