
// local dependencies
import info from './info/infoScreenReducer';
import welcome from './welcome/welcomeScreenReducer';

/**
 * connect all private reducers
 *
 * @public
 */

export default {
    info,
    welcome,
};
