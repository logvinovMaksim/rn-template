
// outsource dependencies
import { fork } from 'redux-saga/effects';

// local dependencies
import info from './info/infoScreenSaga';
import welcome from './welcome/welcomeScreenSaga';

/**
 * connect all private sagas
 *
 * @public
 */
export default function* () {
    yield fork(info);
    yield fork(welcome);
}
