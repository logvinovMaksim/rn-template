
// outsource dependencies
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, View, Button } from 'react-native';
import { connect } from 'react-redux';

// local dependencies
import { APP_OFFSETS } from '../../styles/appThemeConstants';
import { PRIVATE } from '../../actions/types';
import Screen from '../../components/common/Screen';
import Text from '../../components/common/Text';

const styles = StyleSheet.create({
    content: {
        flex: 1,
        justifyContent: 'center',
    },
    getInfoBtn: {
        marginTop: APP_OFFSETS.TOP,
    },
});

class InfoScreen extends PureComponent {
    infoRequest = () => this.props.infoRequest();

    render () {
        const { infoList } = this.props;
        return (
            <Screen>
                <View style={styles.content}>
                    <Text center variant="boldTitle">Welcome to React Native advanced template!</Text>
                    <Text center variant="subtitle">You currently at Info Screen</Text>
                    <View style={styles.getInfoBtn}>
                        <Button title="Get Info" onPress={this.infoRequest} />
                    </View>
                </View>
            </Screen>
        );
    }
}

InfoScreen.displayName = 'InfoScreen';

InfoScreen.propTypes = {
    infoRequest: PropTypes.func.isRequired,
    infoList: PropTypes.array.isRequired,
};

export default connect(
    state => ({ ...state.info }),
    dispatch => ({
        infoRequest: () => dispatch({ type: PRIVATE.INFO.REQUEST }),
    })
)(InfoScreen);
