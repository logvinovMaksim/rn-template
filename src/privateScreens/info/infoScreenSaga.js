
// outsource dependencies
import { takeLatest, put } from 'redux-saga/effects';

//local dependencies
import { PRIVATE } from '../../actions/types';

function* showInfo (action) {
    const { type, ...options } = action;
    try {
        yield put({ type: PRIVATE.INFO.SUCCESS });
    } catch (error) {
        yield put({ type: PRIVATE.INFO.ERROR, errorMessage: error.message });
    }
    yield put({ type: PRIVATE.INFO.FINISH });
}

/**
 * common request handler
 *
 * @public
 */
export default function* () {
    yield takeLatest(PRIVATE.INFO.REQUEST, showInfo);
}
