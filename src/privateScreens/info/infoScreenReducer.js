
// local dependencies
import { PRIVATE } from '../../actions/types';

const initial = {
    expectAnswer: false,
    errorMessage: null,
    infoList: [],
};

export default function (state = initial, action) {
    const { type } = action;
    switch (type) {
        case PRIVATE.INFO.START:
            state = { expectAnswer: true };
            break;
        case PRIVATE.INFO.SUCCESS:
            state = { expectAnswer: false };
            break;
        default:
            break;
    }

    return state;
}
