
// outsource dependencies
import { fork } from 'redux-saga/effects';

// local dependencies
import appWatcher from './app';
import privateWatcher from '../privateScreens/sagas';
import publicWatcher from '../publicScreens/sagas';

/**
 * common root watcher
 *
 * @public
 */
export default function* rootWatcher () {
    yield fork(appWatcher);
    yield fork(publicWatcher);
    yield fork(privateWatcher);
}
