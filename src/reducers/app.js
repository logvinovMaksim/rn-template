
// local dependencies
import { APP } from '../actions/types';

const initial = {
    invalidToken: false,
    expectAnswer: true,
    errorMessage: null,
};

export default function (state = initial, action) {
    const { type } = action;
    switch (type) {
        case APP.INIT.REQUEST:
            state = { expectAnswer: true };
            break;
        case APP.INIT.SUCCESS:
            state = { expectAnswer: false };
            break;
        default:
            break;
    }

    return state;
}
