
// outsource dependencies
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { View, TouchableOpacity, Image, StyleSheet } from 'react-native';

// local dependencies
import { APP_OFFSETS } from '../styles/appThemeConstants';

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
    },
    logo: {
        width: 32,
        height: 32,
        marginLeft: APP_OFFSETS.LEFT,
    },
});

class AppHeader extends PureComponent {
    toggleDrawer = () => this.props.navigation.toggleDrawer();

    render () {
        return (
            <View style={styles.container}>
                <TouchableOpacity onPress={this.toggleDrawer}>
                    <Image
                        source={require('../../assets/favicon-32x32.png')}
                        style={styles.logo}
                    />
                </TouchableOpacity>
            </View>
        );
    }
}

AppHeader.propTypes = {
    navigation: PropTypes.object.isRequired,
};

export default AppHeader;
