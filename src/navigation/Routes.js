
// outsource dependencies
import React from 'react';
import {
    createStackNavigator, createSwitchNavigator, createAppContainer, createDrawerNavigator,
    createBottomTabNavigator
} from 'react-navigation';
import Icon from 'react-native-vector-icons/FontAwesome';

// local dependencies
import StartScreen from '../publicScreens/start/StartScreen';
import SupportScreen from '../publicScreens/support/SupportScreen';
import WelcomeScreen from '../privateScreens/welcome/WelcomeScreen';
import InfoScreen from '../privateScreens/info/InfoScreen';
import CameraScreen from '../publicScreens/camera/CameraScreen';
import { APP_COLORS, HEADER_STYLE } from '../styles/appThemeConstants';
import AppHeader from './AppHeader';

const PublicStack = createStackNavigator(
    {
        StartScreen: {
            screen: StartScreen,
            // insert there params related to navigation state
            params: { userStatus: 'Unknown' },
            navigationOptions: ({ navigation }) => ({
                title: `${navigation.state.params.userStatus} at StartScreen`,
                headerLeft: <AppHeader navigation={navigation} />,
                headerStyle: {
                    backgroundColor: HEADER_STYLE.BACKGROUND_COLOR,
                },
                headerTitleStyle: {
                    color: HEADER_STYLE.TITLE_COLOR,
                },
            }),
        },
        SupportScreen: {
            screen: SupportScreen,
            // insert there params related to navigation state
            params: { userStatus: 'Unknown' },
            navigationOptions: ({ navigation }) => ({
                title: `${navigation.state.params.userStatus} at SupportScreen`,
                headerLeft: <AppHeader navigation={navigation} />,
                headerStyle: {
                    backgroundColor: HEADER_STYLE.BACKGROUND_COLOR,
                },
                headerTitleStyle: {
                    color: HEADER_STYLE.TITLE_COLOR,
                },
            }),
        },
        CameraScreen: {
            screen: CameraScreen,
            navigationOptions: {
                header: null,
            },
        }
    },
    // some general settings for stack navigator header
    // uncomment to enable
    // {
    //     // headerMode: 'none',
    //     headerLayoutPreset: 'center',
    //     mode: 'modal',
    //     defaultNavigationOptions: {
    //         headerLayoutPreset: 'center',
    //         headerStyle: {
    //             backgroundColor: '#723',
    //         },
    //     }
    // }
);

const PrivateStack = createStackNavigator(
    {
        WelcomeScreen: {
            screen: WelcomeScreen,
            params: { userStatus: 'Unknown' },
            navigationOptions: ({ navigation }) => ({
                title: `${navigation.state.params.userStatus} at WelcomeScreen`,
                headerLeft: <AppHeader navigation={navigation} />,
                headerStyle: {
                    backgroundColor: HEADER_STYLE.BACKGROUND_COLOR,
                },
                headerTitleStyle: {
                    color: HEADER_STYLE.TITLE_COLOR,
                },
            }),
        },
        InfoScreen: {
            screen: InfoScreen,
            params: { userStatus: 'Unknown' },
            navigationOptions: ({ navigation }) => ({
                title: `${navigation.state.params.userStatus} at InfoScreen`,
                headerLeft: <AppHeader navigation={navigation} />,
                headerStyle: {
                    backgroundColor: HEADER_STYLE.BACKGROUND_COLOR,
                },
                headerTitleStyle: {
                    color: HEADER_STYLE.TITLE_COLOR,
                },
            }),
        }
    },
    {
        // headerMode: 'none',
        initialRouteName: 'InfoScreen',
    }
);

const TabNavigator = createBottomTabNavigator(
    {
        PrivateTab: PrivateStack,
        PublicTab: PublicStack,
    },
    {
        defaultNavigationOptions: ({ navigation }) => ({
            // eslint-disable-next-line
            tabBarIcon: ({ focused, horizontal, tintColor = APP_COLORS.THEME_DISABLED_COLOR }) => {
                const { routeName } = navigation.state;
                let iconName = 'tree';
                if (routeName === 'PrivateTab') {
                    // your code
                }
                if (routeName === 'PublicTab') {
                    iconName = 'heart';
                }
                return (<Icon name={iconName} size={20} color={tintColor} />);
            },
        }),
        initialRouteName: 'PublicTab',
        tabBarOptions: {
            activeTintColor: APP_COLORS.THEME_LIGHT_COLOR,
            inactiveTintColor: APP_COLORS.THEME_DISABLED_COLOR,
            activeBackgroundColor: APP_COLORS.THEME_COLOR,
            inactiveBackgroundColor: APP_COLORS.THEME_LIGHT_COLOR,
        },
    });

const DrawerNavigatorExample = createDrawerNavigator({
    //Drawer Options and indexing
    PublicDrawer: {
        screen: PublicStack,
        navigationOptions: {
            drawerLabel: 'PublicDrawer',
            drawerIcon: <Icon name="thumbs-down" size={30} color={APP_COLORS.NEGATIVE_RED} />,
        },
    },
    PrivateDrawer: {
        screen: PrivateStack,
        navigationOptions: {
            drawerLabel: 'PrivateDrawer',
            drawerIcon: <Icon name="tree" size={30} color={APP_COLORS.NEGATIVE_RED} />,
        },
    },
    TabNavigator: {
        screen: TabNavigator,
        navigationOptions: {
            drawerLabel: 'TabNavigator',
            drawerIcon: <Icon name="thumbs-up" size={30} color={APP_COLORS.NEGATIVE_RED} />,
        },
    }
});

// Another nav option
// export const AppRoutes = createSwitchNavigator(
//     {
//         Public: PublicStack,
//         Private: PrivateStack,
//     }
// );

export default createAppContainer(DrawerNavigatorExample);
